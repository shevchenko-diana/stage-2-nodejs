const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    getAll() {
        const item = FightRepository.getAll();
        if(!item) {
            return null;
        }
        return item;
    }

    save(fight){
        const item = FightRepository.create(fight)
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FightersService();
