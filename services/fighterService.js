const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }
    searchById(id) {
        const item = FighterRepository.getOne(function (element) {
            return element.id === id
        });
        if(!item) {
            return null;
        }
        return item;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    save(fighter){
        const item = FighterRepository.create(fighter);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate){
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();
