const {Router} = require('express');
const FightService = require('../services/fightService');
const {responseMiddleware} = require('../middlewares/response.middleware');


const router = Router();

router.get('/', (req, res, next) => {
    try {
        let fights = FightService.getAll()
        if (!fights) {
            throw  {err: 'No fights found', statusCode: 404};
        }

        res.data = fights
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }


}, responseMiddleware);
router.get('/:id', (req, res, next) => {
    try {
        let fight = FightService.search(function (element) {
            return element.id === req.params.id
        });
        if (!fight) {
            throw  {err: 'No fight found', statusCode: 404};
        }

        res.data = fight
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);
router.get('/fighter/:id', (req, res, next) => {
    try {
        let fight = FightService.search(function (element) {
            return element.fighter1 === req.params.id || element.fighter2 === req.params.id
        });
        if (!fight) {
            throw  {err: 'No fight found', statusCode: 404};
        }

        res.data = fight
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);
router.post('/', (req, res, next) => {
    try {
        let fight = FightService.save({
            fighter1: req.body.fighter1,
            fighter2: req.body.fighter2,

        });
        if (!fight) {
            throw  {err: 'Could not save a fight', statusCode: 404};
        }

        res.data = fight
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
