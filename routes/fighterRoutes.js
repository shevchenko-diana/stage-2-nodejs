const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');
const router = Router();


router.get("/", function (req, res) {
    let fighters = FighterService.getAll();
    if (!fighters) {
        res.status(404).send(('No fighters found'))

    }
    res.status(200).send(fighters)
});

router.post("/", createFighterValid, function (req, res) {
    let fighterObj = req.body;
    let fighter = FighterService.search(function (element) {
        return stripName(element.name) === stripName(fighterObj.name)
    });

    if (fighter) {
        res.status(400).send(('Fighter with such name already exists'));
        return
    }

    fighter = FighterService.save({
        name: fighterObj.name,
        power: fighterObj.power,
        defense: fighterObj.defense,
        health: fighterObj.health ? fighterObj.health : 100,
    });

    if (!fighter) {
        res.status(400).send(('Something went wrong'));
        return;
    }

    res.send(fighter)

});


router.get('/:id', (req, res, next) => {
    try {

        let fighter = FighterService.searchById(req.params.id);

        if (!fighter) {
            throw {err: 'Fighter not found', statusCode: 404};
        }

        res.data = fighter;

    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {

        let fighter = FighterService.search(function (element) {
            return element.email === req.body.email ||
                element.phoneNumber === req.body.phoneNumber
        });
        if (!!fighter && fighter.id !== req.params.id) throw  {
            err: 'Fighter with such email or phone already exists',
            statusCode: 400
        };

        fighter = FighterService.searchById(req.params.id);
        if (!fighter) throw  {err: 'Fighter not found', statusCode: 404};

        fighter = FighterService.update(req.params.id, stripData([{...fighter, ...req.body}])[0]);

        if (!fighter) {
            throw {err: 'Something went wrong', statusCode: 400}
        }

        res.data = fighter;

    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let fighter = FighterService.searchById(req.params.id);
        if (!fighter) throw  {err: 'Fighter not found', statusCode: 404};

        fighter = FighterService.delete(req.params.id);
        if (!fighter) {
            throw {err: 'Something went wrong', statusCode: 400}
        }

        res.data = {message: 'Ok'}
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

const stripData = function (fighters) {
    return fighters.map(obj => ({
        id: obj.id,
        name: obj.name,
        power: obj.power,
        defense: obj.defense,
        createdAt: obj.createdAt,
        updatedAt: obj.updatedAt,
    }))

};

const stripName = function (name) {
    return name.toLowerCase().trim()
}
module.exports = router;
