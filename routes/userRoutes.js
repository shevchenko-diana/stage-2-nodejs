const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');
const router = Router();

router.get('/', (req, res, next) => {
    try {
        let users = UserService.getAll();

        if (!users || users.length ==0) {
            throw {err: 'No users found', statusCode: 404};
        }

        res.data = userToDto(users);

    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {

        let user = UserService.searchById(req.params.id);

        if (!user) {
            throw {err: 'User not found', statusCode: 404};
        }

        res.data = userToDto([user])[0];

    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        let obj = req.body;

        let user = UserService.search(function (element) {
            return stripEmail(element.email) === stripEmail(obj.email) ||
                element.phoneNumber === obj.phoneNumber
        });
        if (user) throw {err: 'User with such email or phone already exists', statusCode: 400};

        user = UserService.save({
            firstName: obj.firstName,
            lastName: obj.lastName,
            email: obj.email,
            phoneNumber: obj.phoneNumber,
            password: obj.password,
        });

        if (!user) {
            throw {err: 'Something went wrong', statusCode: 400}
        }
        res.data = user
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {

        let user = UserService.search(function (element) {
            return element.email === req.body.email ||
                element.phoneNumber === req.body.phoneNumber
        });
        if (!!user && user.id !== req.params.id) throw  {
            err: 'User with such email or phone already exists',
            statusCode: 400
        };

        user = UserService.searchById(req.params.id);
        if (!user) throw  {err: 'User not found', statusCode: 404};

        user = UserService.update(req.params.id, stripData([{...user, ...req.body}])[0]);

        if (!user) {
            throw {err: 'Something went wrong', statusCode: 400}
        }

        res.data = userToDto([user])[0];

    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let user = UserService.searchById(req.params.id);
        if (!user) throw  {err: 'User not found', statusCode: 404};

        user = UserService.delete(req.params.id);
        if (!user) {
            throw {err: 'Something went wrong', statusCode: 400}
        }

        res.data = {message: 'Ok'}
    } catch (err) {
        res.err = err.err;
        res.statusCode = err.statusCode
    } finally {
        next();
    }
}, responseMiddleware);

const stripData = function (users) {
    return users.map(obj => ({

        id: obj.id,
        firstName: obj.firstName,
        lastName: obj.lastName,
        email: obj.email,
        phoneNumber: obj.phoneNumber,
        password: obj.password,
        createdAt: obj.createdAt,
        updatedAt: obj.updatedAt,
    }))

};
//to remove sensitive or surplus data
const userToDto = function (users) {
    return users.map(obj => ({

        id: obj.id,
        firstName: obj.firstName,
        lastName: obj.lastName,
        email: obj.email,
        phoneNumber: obj.phoneNumber,
    }))

};

//todo remove duplicate
const stripEmail = function (email) {
    return email.toLowerCase().trim()
}
module.exports = router;

