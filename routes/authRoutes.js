const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        let user = AuthService.login({email: req.body.email, password: req.body.password})

        if (!user) {
            throw {err: 'Invalid credentials', statusCode: 400};
        }
        res.data = {message: 'Ok'}

    } catch (err) {
        res.err = 'Invalid credentials';
        res.statusCode = 400
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
