const responseMiddleware = (req, res, next) => {

    if (res.err) {
        res.status(res.statusCode).send(newError(res.err));
        return
    }
    res.status(200).send(res.data);

    next();
};

const newError = function (msg) {
    return {
        error: true,
        message: msg
    }
};

exports.responseMiddleware = responseMiddleware;
