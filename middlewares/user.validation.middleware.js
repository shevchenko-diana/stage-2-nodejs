const {user} = require('../models/user');
let phoneRegex = new RegExp('\\+380[0-9]{9}');
let nameRegex = new RegExp('[A-Za-z\s]+');
let passwordRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
let emailRegex = new RegExp('[a-zA-Z0-9.]+@gmail.com');

const createUserValid = (req, res, next) => {
    let {email, phoneNumber, firstName, lastName, password, id} = req.body;
    let msg = '';

    if (!!id){
        msg = 'Id should not be present'
    }
    switch (true) {
        case !emailRegex.test(email):
            msg = 'User must have gmail.com email';
            break;
        case !nameRegex.test(firstName) || !nameRegex.test(lastName):
            msg = 'Only alphabetical characters allowed';
            break;
        case !phoneRegex.test(phoneNumber):
            msg = 'Phone must be +380xxxxxxxxx';
            break;
        case !passwordRegex.test(password):
            msg = 'Password must be stronger';
            break
    }
    if (msg) {
        res.status(400).send(JSON.stringify({
            error: true,
            message: msg
        }));
        return
    }


    next();
};

const updateUserValid = (req, res, next) => {

    let {email, phoneNumber, firstName, lastName, password} = req.body;
    let msg = '';
    switch (true) {
        case !!email && !emailRegex.test(email):
            msg = 'User must have gmail.com email';
            break;
        case !!firstName && !nameRegex.test(firstName) :
            msg = 'Only alphabetical characters allowed';
            break;
        case !!lastName && !nameRegex.test(lastName) :
            msg = 'Only alphabetical characters allowed';
            break;
        case !!phoneNumber && !phoneRegex.test(phoneNumber):
            msg = 'Phone must be +380xxxxxxxxx';
            break;
        case !!password && !passwordRegex.test(password):
            msg = 'Password must be stronger';
            break
    }
    if (msg) {
        res.status(400).send(JSON.stringify({
            error: true,
            message: msg
        }));
        return
    }
    next();
};


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
