const {fighter} = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    let {name, power, defense} = req.body;
    let msg = generalFighterValidation(name, power, defense);

    if (msg) {
        res.status(400).send(JSON.stringify({
            error: true,
            message: msg
        }));
        return
    }

    next();
};

const updateFighterValid = (req, res, next) => {
    let {name, power, defense} = req.body;
    let msg = generalFighterValidation(name, power, defense);

    if (msg) {
        res.status(400).send(JSON.stringify({
            error: true,
            message: msg
        }));
        return
    }

    next();
};

const generalFighterValidation = (name, power, defense) => {
    let msg = '';
    switch (true) {
        case !(!!name && RegExp('^[A-Za-z]+$').test(name)):
            msg = 'Name can have only alphabetical symbols';
            break;
        case !(parseFloat(power) === +power && power < 100 && power > 0):
            msg = 'Power must be between 0 and 100 exclusive';
            break;
        case !(parseFloat(defense) === +defense && defense < 100 && defense >= 0):
            msg = 'Defense must be between 0 and 100';
            break
    }
    return msg

}


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
